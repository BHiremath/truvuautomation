package com.automation.truvu.TruVuAutomation.ObjectFactory;

import com.automation.truvu.TruVuAutomation.ObjectFactory.Pages.HomePage;
import com.automation.truvu.TruVuAutomation.ObjectFactory.Pages.LoginPage;

public class TruVuObjectFactory {
	
private static final LoginPage loginPage = null;
private static final HomePage homePage = null;

	
	public static LoginPage getLoginPage() {
		if(loginPage == null) return new LoginPage();
		
		return loginPage;
	}
	
	public static HomePage getHomePage() {
		if(homePage == null) return new HomePage();
		
		return homePage;
	}
	
	

}
