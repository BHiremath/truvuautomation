package com.automation.truvu.TruVuAutomation.ObjectFactory.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.automation.truvu.TruVuAutomation.BusinessFunctions.Application;

public class HomePage {
	
	Logger logger = LoggerFactory.getLogger("HomePage");
	
	public void Logout() {
		try {
			final WebDriver driver = Application.getDriver();
			//TODO change to conditional wait
			isSpinnerDisplayed();
			driver.findElement(By.id("logout")).click();
			logger.info("Logout button exists and clicked.");
		}catch(Exception e) {
			Assert.fail("Failed to Logout");
			logger.error("Failed to Logout", e.getMessage());
		}
		
		
	}
	
	public boolean isSpinnerDisplayed() {
		try {
			final WebDriver driver = Application.getDriver();
			while(driver.findElement(By.xpath("(//div[contains(@class, 'spinner')])[1]")).isDisplayed()) {
				logger.info("still displaying spinner..");
				isSpinnerDisplayed();
			}
		}catch(Exception e) {
			Assert.fail("Failed while checking spinner status");
			logger.error("Failed while checking spinner status", e.getMessage());
		}
		
		return true;
	}

}
