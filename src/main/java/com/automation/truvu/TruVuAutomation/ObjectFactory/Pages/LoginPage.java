package com.automation.truvu.TruVuAutomation.ObjectFactory.Pages;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.automation.truvu.TruVuAutomation.BusinessFunctions.Application;

public class LoginPage {
	
	Logger logger = LoggerFactory.getLogger("LoginPage");

	public void Launchurl(String url) {	

		try {
			final WebDriver driver = Application.getDriver();

			driver.manage().window().maximize();
			// Launch Website			
			driver.navigate().to(url);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
			
			Thread.sleep(2 * 1000);
			
			WebElement Ok = driver.findElement(By.xpath("(//button[text()='OK'])[1]"));
			boolean isCookieExist = Ok.isDisplayed();
			if (isCookieExist) {
				Ok.click();
				Reporter.log("Coockie button clicked successfully");

			} else {
				Assert.fail("Coockie button not clicked");
			}

		} catch (Exception e) {
			Assert.fail("Failed to Launchurl");
			logger.error("Failed to Launchurl", e.getMessage());
		}		
	}

	public void Login(final String user, final String pass) {
		try {

			final WebDriver driver = Application.getDriver();

			driver.findElement(By.id("email")).sendKeys(user);
			driver.findElement(By.id("password")).sendKeys(pass);

			driver.findElement(By.id("login")).click();

			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			
			if (driver.findElements(By.id("logout")).size() != 0) {
				Reporter.log("Login to TruVu application successfull");
			} else {
				Assert.fail("Login to TruVu application failed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to Login");
			logger.error("Failed to Login", e.getMessage());
		}
		
	}
	
	public void VerifyLogout() {	
		
		try {
			final WebDriver driver = Application.getDriver();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			Thread.sleep(2 * 1000);
			
			WebElement checkpoint = driver.findElement(By.xpath("//h4[text()='Login']"));
			boolean isCheckPointExist = checkpoint.isDisplayed();
			if (isCheckPointExist) {
				Reporter.log("Logout from TruVu application successfull");
			} else {
				Assert.fail("Logout from TruVu application failed");
			}
		} catch (Exception e) {
			Assert.fail("Logout from TruVu application failed, failed to locate login name");
			logger.error("Failed to VerifyLogout", e.getMessage());
		}		
		
	}

	

}
