package com.automation.truvu.TruVuAutomation.BusinessFunctions.Applications;

import com.automation.truvu.TruVuAutomation.ObjectFactory.TruVuObjectFactory;

public class TruVuFunctions {

	public void Launchurl(String url) {

		TruVuObjectFactory.getLoginPage().Launchurl(url);
	}

	public void Login(final String user, final String pass) {

		TruVuObjectFactory.getLoginPage().Login(user, pass);
		
	}

	public void Logout() {

		TruVuObjectFactory.getHomePage().Logout();
		TruVuObjectFactory.getLoginPage().VerifyLogout();
	}
	
	
	
	

}
