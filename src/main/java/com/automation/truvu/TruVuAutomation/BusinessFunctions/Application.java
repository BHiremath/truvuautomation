package com.automation.truvu.TruVuAutomation.BusinessFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.automation.truvu.TruVuAutomation.BusinessFunctions.Applications.TruVuFunctions;

public class Application {

	private static TruVuFunctions truVuFunctions = null;
	private static WebDriver driver = null;
	
	
	public static TruVuFunctions getTruVuFunctions() {
		if(truVuFunctions == null) return new TruVuFunctions();
		
		return truVuFunctions;
	}
	
	
	
	public static WebDriver getDriver() {
		if(driver != null) {
			return driver;			
		}
		
		//add switch case to support multiple browser types
		System.setProperty("webdriver.chrome.driver", "D:\\driver\\chromedriver.exe");

		// Instantiate a ChromeDriver class.
		driver = new ChromeDriver();
		
		return driver;
	}

}
