package com.automation.truvu.TruVuAutomation.TestData.Input;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestData {
	static Logger logger = LoggerFactory.getLogger("TestData");

	private static Properties prop = null;


	public static String getDataFor(final String key) {
		String val = "";
		try {
			if (prop == null) {
				InputStream is = TestData.class.getResourceAsStream("truvu.properties");
				prop = new Properties();
				prop.load(is); 
			}
			
			val = prop.getProperty(key);
			
		} catch (IOException e) {
			logger.error("Could not find properties file.");
		} 

		return val;
	}

}
