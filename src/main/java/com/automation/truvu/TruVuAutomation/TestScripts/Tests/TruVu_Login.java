// ######################################################################################
// # TEST SCRIPT NAME   : TruVu Login
// # OBJECTIVE			: Login page - Show Dashboard 
// # AUTHOOR			:
// # DATE				: 
// # PRE-CONDITIONS		:
// # EPIC DETAILS		: 
// ######################################################################################



package com.automation.truvu.TruVuAutomation.TestScripts.Tests;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.automation.truvu.TruVuAutomation.BusinessFunctions.Application;
import com.automation.truvu.TruVuAutomation.TestData.Input.TestData;

public class TruVu_Login {
	
	@Test
	public void TruVu_Login() {
		
		int StepNo = 1;
		String url = TestData.getDataFor("url1");
		String user = TestData.getDataFor("user1");
		String pass = TestData.getDataFor("password1");
		
		Reporter.log("Verify user has logged in with valid user id and pasword", true);
		
		Reporter.log("Step: "+ (StepNo++) +" Launch TruVu application",true);
		Application.getTruVuFunctions().Launchurl(url);
		
		Reporter.log("Step: "+ (StepNo++) +" Login into TruVu application",true);
		Application.getTruVuFunctions().Login(user, pass);
		
		Reporter.log("Step: "+ (StepNo++) +" Logout from TruVu application",true);
		Application.getTruVuFunctions().Logout();
		
		
		
	}

}
